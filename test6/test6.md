﻿﻿﻿﻿﻿﻿﻿# 实验6（期末考核） 基于Oracle数据库的售卖系统
**姓名：陈建洋**

**学号：202010414402**

**班级：20软工四班**

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
- 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
- 设计权限及用户分配方案。至少两个用户。
- 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
- 设计一套数据库的备份方案。

## 实验内容
1. 实现表的结构和业务具体逻辑 

   ​		第一步，我们需要确定商品销售系统的核心功能和数据存储需求。我们可以假设该系统由四个主要表格组成，分别是订单表、客户表、商品表和库存表。订单表用于记录每个订单的详细信息，客户表则用于保存客户的基本信息。商品表用于存储商品的相关信息，而库存表则记录了商品的库存量和当前价格。此外，系统还需要设计复杂的业务逻辑，如订单退款/退货流程、优惠券/促销活动等。

   

2. 设计表空间

   ​		为了有效地管理数据和索引文件，我们选择了使用两个表空间。第一个表空间名为"sales_tablespace"，用于存储数据文件，而第二个表空间名为"indexs_tablespace"，用于存储索引文件。在创建表时，我们需要指定所属的表空间，以便根据不同表的大小和访问频率进行灵活的管理。每个表空间都关联了一个数据文件（DATAFILE），并且我们设置了初始大小（SIZE）、自动扩展（AUTOEXTEND）功能，以及下一个增长阶段的大小（NEXT）和最大大小（MAXSIZE）限制。这样的设置能够满足不同表的数据存储需求，并且确保表空间在需要时可以自动扩展，同时限制了其最大大小以避免过度占用存储资源。

   ```sql
   
   CREATE TABLESPACE sales_tablespace
     DATAFILE 'sales_tablespace.dbf'
     SIZE 100M
     AUTOEXTEND ON
     NEXT 10M
     MAXSIZE UNLIMITED;
   
   CREATE TABLESPACE indexs_tablespace
     DATAFILE 'indexs_tablespace.dbf'
     SIZE 50M
     AUTOEXTEND ON
     NEXT 10M
     MAXSIZE UNLIMITED;
   
   ALTER USER sale_admin DEFAULT TABLESPACE sales_tablespace;
   ALTER USER sale_admin DEFAULT TABLESPACE indexs_tablespace;
   ```

   ![Alt text](1.png)
   

   

   ```sql
   
   ALTER USER sale_admin QUOTA UNLIMITED ON sales_tablespace;
   ALTER USER sale_admin QUOTA UNLIMITED ON temp_tablespace;
   ```

   ![Alt text](2.png)

   

   接下来我们创建了四个表。首先是"PRODUCTS"表，用于存储产品信息。该表包含了字段如产品ID（productid）、产品名称（productname）、价格（price）和数量（quantity）等。

   其次是"ORDERS"表，用于存储订单信息。该表包含了字段如订单ID（orderid）、下单日期（orderdate）和顾客ID（customerid）等。同时，我们还定义了一个外键引用"CUSTOMER"表，以建立订单和顾客之间的关联。

   第三个是"ORDERTETAILS"表，用于存储订单细节信息。该表包含了字段如订单ID（orderid）、产品ID（productid）、产品数量（quantity）和产品价格（price）等。为了保证订单和产品之间的唯一性，我们定义了一个复合主键（PRIMARY KEY）。

   最后是"CUSTOMER"表，用于存储顾客信息。该表包含了字段如顾客ID（customerid）、顾客姓名（customername）和电子邮件地址（email）等。

      ``` sql
      
      CREATE TABLE PRODUCTS (
        productid   NUMBER PRIMARY KEY,
        productname VARCHAR2(100) NOT NULL,
        price        NUMBER(10, 2) NOT NULL,
        quantity     NUMBER
      )
      TABLESPACE sales_tablespace;
      
      
      CREATE TABLE ORDERS (
        orderid    NUMBER PRIMARY KEY,
        orderdate  DATE NOT NULL,
        customerid NUMBER REFERENCES customer(customerid)
      )
      TABLESPACE sales_tablespace;
      
      
      CREATE TABLE ORDERTETAILS (
        orderid   NUMBER REFERENCES orders(orderid),
        productid NUMBER REFERENCES products(productid),
        quantity   NUMBER,
        price      NUMBER(10, 2),
        PRIMARY KEY (orderid, productid)
      )
      TABLESPACE sales_tablespace;
      
      
      CREATE TABLE CUSTOMER (
        customerid   NUMBER PRIMARY KEY,
        customername VARCHAR2(100) NOT NULL,
        email         VARCHAR2(100)
      )
      TABLESPACE sales_tablespace;
      ```

     ![Alt text](3.png)

   授予名为`sale_admin`的用户对四个表(PRODUCTS、ORDERS、ORDERTETAILS和CUSTOMER)的SELECT、INSERT、UPDATE和DELETE权限。

   ```
   GRANT SELECT, INSERT, UPDATE, DELETE ON products TO sale_admin;
   GRANT SELECT, INSERT, UPDATE, DELETE ON ORDERS TO sale_admin;
   GRANT SELECT, INSERT, UPDATE, DELETE ON ORDERTETAILS TO sale_admin;
   GRANT SELECT, INSERT, UPDATE, DELETE ON CUSTOMER TO sale_admin;
   ```

   ![Alt text](4.png)

   接下来我们将向"PRODUCTS"表插入虚拟数据，总共插入30,000条记录。具体步骤如下：

   我们使用一个FOR循环语句来迭代从1到30,000的整数，每次迭代都代表一条记录。
   在每次迭代中，我们使用INSERT INTO语句将一条新记录插入"PRODUCTS"表中。插入的字段包括"productid"（即当前迭代的整数值）、"productname"（格式为"Product i"，其中i表示当前迭代的整数值）、"price"（在10到100之间随机生成的价格）和"quantity"（在1到100之间随机生成的数量）。
   在插入之前，我们使用SELECT语句从"dual"表中选择一行，并使用子查询来检查是否已经存在具有相同"productid"的记录。如果存在相同的"productid"记录，则不执行插入操作。
   为了避免事务过大，我们在每100次迭代之后执行一次COMMIT提交操作，以将数据持久化到数据库。
   
   BEGIN
     FOR i IN 1..30000 LOOP
       INSERT INTO products (productid, productname, price, quantity)
       SELECT i AS productid, 'Product ' || i AS productname, ROUND(DBMS_RANDOM.VALUE(10, 100), 2) AS price, ROUND(DBMS_RANDOM.VALUE(1, 100)) AS quantity
       FROM dual
       WHERE NOT EXISTS (SELECT 1 FROM products WHERE productid = i);
     
       -- Commit periodically to avoid excessive transaction size
       IF MOD(i, 100) = 0 THEN
         COMMIT;
       END IF;
     END LOOP;
     COMMIT;
   END;
   /
   ```

   ![Alt text](5.png)

   为了向"CUSTOMER"表插入虚拟数据，我们可以按照以下步骤进行：

   使用FOR循环语句，从1到30,000遍历每个整数i。

   在每次迭代中，使用INSERT INTO语句将一条新记录插入"CUSTOMER"表。这条记录包括字段customerid（即i）、customername（格式为"Customer i"）和email（格式为"customeri@example.com"）。

   同时，使用SELECT语句从"dual"表中选择一行，并使用子查询来检查是否已经存在具有相同customerid的记录。如果存在相同的customerid记录，则不执行插入操作。
   BEGIN
     FOR i IN 1..30000 LOOP
       INSERT INTO customer (customerid, customername, email)
       SELECT i AS customerid, 'Customer ' || i AS customername, 'customer' || i || '@example.com' AS email
       FROM dual
       WHERE NOT EXISTS (
         SELECT 1
         FROM customer
         WHERE customerid = i
       );
     END LOOP;
     COMMIT;
   END;
   /
   ```

   ![Alt text](6.png)

   为了向"ORDERS"表插入虚拟数据，我们可以按照以下步骤进行操作：

   使用FOR循环语句，从1到30,000遍历每个整数i。
   在每个迭代中，使用INSERT INTO语句将一条新记录插入"ORDERS"表中。该记录包含了orderid（即i）、orderdate（通过在当前日期基础上随机减去1到365之间的天数生成）和customerid（在1到1000之间随机选择）等字段。
   使用SELECT语句从"dual"表中选择一行，并使用子查询检查是否已经存在具有相同orderid的记录。如果存在相同orderid的记录，则不执行插入操作。
   不需要在循环外显式提交事务，因为每次循环都会作为一个单独的事务处理。
   BEGIN
     FOR i IN 1..30000 LOOP
       INSERT INTO orders (orderid, orderdate, customerid)
       SELECT i AS orderid, SYSDATE - TRUNC(DBMS_RANDOM.VALUE(1, 365)) AS orderdate, ROUND(DBMS_RANDOM.VALUE(1, 1000)) AS customerid
       FROM dual
       WHERE NOT EXISTS (
         SELECT 1
         FROM orders
         WHERE orderid = i
       );
     END LOOP;
     COMMIT;
   END;
   /
   ````
   
   ![Alt text](7.png)

   
   我们将使用以下流程向"ORDERTETAILS"表插入虚拟数据：

   我们将使用FOR循环语句，从1到30,000遍历每个整数i。

   在每个迭代中，我们将使用MERGE INTO语句将一条新记录插入"ORDERTETAILS"表中。首先，我们将使用SELECT语句从"dual"表中选择一行，并生成具有以下字段的数据：orderid（即i）、productid（在1到30,000之间随机选择）、quantity（在1到10之间随机选择）和price（在10到100之间随机生成）。

   然后，我们将使用MERGE INTO语句将这条数据与"ORDERTETAILS"表进行合并。如果存在orderid和productid都匹配的记录，则不执行任何操作。否则，将插入一条新记录，其中包含orderid、productid、quantity和price。
   
   ```sql
   BEGIN
     FOR i IN 1..30000 LOOP
       MERGE INTO ORDERTETAILS od
       USING (
         SELECT i AS orderid, ROUND(DBMS_RANDOM.VALUE(1, 30000)) AS productid, ROUND(DBMS_RANDOM.VALUE(1, 10)) AS quantity, ROUND(DBMS_RANDOM.VALUE(10, 100), 2) AS price
         FROM dual
       ) data
       ON (od.orderid = data.orderid AND od.productid = data.productid)
       WHEN NOT MATCHED THEN
         INSERT (orderid, productid, quantity, price)
         VALUES (data.orderid, data.productid, data.quantity, data.price);
     END LOOP;
     COMMIT;
   END;
   /
   ```
   
   ![Alt text](8.png)


3. 创建授权和用户 

   ​		建立两个用户：管理员用户和普通用户。管理员用户拥有对所有表的完全访问权限，而普通用户只有部分访问权限。在授权时要考虑到安全性和灵活性。

   ```sql
   
   CREATE USER sale_admin IDENTIFIED BY 123;
   
   GRANT CONNECT, RESOURCE TO sale_admin;
   ```

   ![Alt text](9.png)

   

   创建普通用户

   ```sql
   CREATE USER sale_user IDENTIFIED BY 123;
   ```

   ![Alt text](10.png)

4. 完成函数和存储过程 
   我们创建了一个名为"sales_pkg"的包，其中包含了一个名为"print_order_summary"的PROCEDURE子程序。该子程序接受一个订单ID作为输入参数，并打印有关该订单的信息。打印的信息包括订单的总金额、订单日期，以及订单中每个商品的名称、数量和单价。

   ```
   CREATE OR REPLACE PACKAGE sales_pkg AS
     PROCEDURE print_order_summary(p_orderid IN NUMBER);
   END;
   /
   
   CREATE OR REPLACE PACKAGE BODY sales_pkg AS
     PROCEDURE print_order_summary(p_orderid IN NUMBER) IS
       l_order_total NUMBER := 0;
       l_order_date DATE;
       l_product_name VARCHAR2(100);
       l_quantity NUMBER;
       l_price NUMBER(10, 2);
     BEGIN
       
       SELECT SUM(od.quantity * p.price)
       INTO l_order_total
       FROM orders o
       JOIN ordertetails od ON o.orderid = od.orderid
       JOIN products p ON od.productid = p.productid
       WHERE o.orderid = p_orderid;
       
       
       SELECT orderdate
       INTO l_order_date
       FROM orders
       WHERE orderid = p_orderid;
       
       
       DBMS_OUTPUT.PUT_LINE('Order Total: $' || l_order_total);
       DBMS_OUTPUT.PUT_LINE('Order Date: ' || TO_CHAR(l_order_date, 'MM/DD/YYYY HH24:MI:SS'));
       
       
       FOR r IN (
         SELECT p.productname, od.quantity, od.price
         FROM orders o
         JOIN ordertetails od ON o.orderid = od.orderid
         JOIN products p ON od.productid = p.productid
         WHERE o.orderid = p_orderid
       ) LOOP
         
         DBMS_OUTPUT.PUT_LINE(r.productname || ' (' || r.quantity || ') - $' || r.price);
       END LOOP;
     EXCEPTION
       WHEN NO_DATA_FOUND THEN
         DBMS_OUTPUT.PUT_LINE('Order not found');
     END;
   END;
   /
   ```

   ![Alt text](12.png)

   为验证程序包的可执行性。下面是程序包的简单调用示例：

   ```
   DECLARE
     P_ORDERID NUMBER;
   BEGIN
     P_ORDERID := 4332;
   
     SALES_PKG.PRINT_ORDER_SUMMARY(
       P_ORDERID => P_ORDERID
     );
   --rollback; 
   END;
   ```
   ![Alt text](11.png)
   

   ​		创建一个名为sales_pkg3的包，其中包含PROCEDURE子程序print_orders_by_customer。此子程序将接受一个顾客ID作为输入参数，然后按订单日期升序输出该顾客所有订单的摘要信息，包括每个订单的日期和总金额。

   ``` 
   CREATE OR REPLACE PACKAGE sales_pkg3 AS
     PROCEDURE print_orders_by_customer(p_customerid IN NUMBER);
   END;
   /
   
   CREATE OR REPLACE PACKAGE BODY sales_pkg3 AS
     PROCEDURE print_orders_by_customer(p_customerid IN NUMBER) IS
       l_orderdate DATE;
       l_order_total NUMBER;
     BEGIN
       FOR r IN (
         SELECT o.orderid, o.orderdate
         FROM orders o
         WHERE o.customerid = p_customerid
       ) LOOP
        
         l_orderdate := r.orderdate;
         
         
         SELECT SUM(od.quantity * p.price)
         INTO l_order_total
         FROM ordertetails od
         JOIN products p ON od.productid = p.productid
         WHERE od.orderid = r.orderid;
         
        
         DBMS_OUTPUT.PUT_LINE('Order Date: ' || TO_CHAR(l_orderdate, 'MM/DD/YYYY HH24:MI:SS'));
         DBMS_OUTPUT.PUT_LINE('Order Total: $' || l_order_total);
       END LOOP;
     EXCEPTION
       WHEN NO_DATA_FOUND THEN
         DBMS_OUTPUT.PUT_LINE('Orders not found');
     END;
   END;
   /
   ```

   为验证程序包的可执行性。下面是程序包的简单调用示例：

   ```` 
   DECLARE
     P_CUSTOMERID NUMBER;
   BEGIN
     P_CUSTOMERID := 45;
   
     SALES_PKG3.PRINT_ORDERS_BY_CUSTOMER(
       P_CUSTOMERID => P_CUSTOMERID
     );
   --rollback; 
   END;
   ````

   
   ![Alt text](13.png)
   ​		

5. 完成数据库的备份 

   ​		为确保数据的安全性和可恢复性，我们设计了一个数据库备份方案。首先，我们将数据库切换到归档日志模式，这样可以确保所有的日志记录都被保存下来。然后，我们使用RMAN（Recovery Manager）对整个数据库进行备份。

   我们定义了一个RMAN备份脚本，该脚本能够完整备份整个数据库。脚本包括了自动备份控制文件、备份集的保留策略、备份优化以及删除过时备份集等设置。

   通过这个备份方案，我们能够定期对数据库进行完整备份，确保数据的安全性。同时，备份集的保留策略和备份优化设置可以帮助我们有效管理备份文件的存储空间和备份过程的效率。在需要恢复数据库时，我们可以使用这些备份文件进行恢复操作，从而保证数据的可恢复性。


```sql

sqlplus / as sysdba

SHUTDOWN IMMEDIATE

STARTUP MOUNT

ALTER DATABASE ARCHIVELOG；

ALTER DATABASE OPEN；
```
![Alt text](14.png)


```sql
rman target /
SHOW ALL;

BACKUP AS COMPRESSED BACKUPSET DATABASE PLUS ARCHIVELOG;

LIST BACKUP;
```
![Alt text](15.png)
![Alt text](16.png)
![Alt text](17.png)

```sql

RUN {
  CONFIGURE CONTROLFILE AUTOBACKUP ON;
  CONFIGURE CONTROLFILE AUTOBACKUP FORMAT FOR DEVICE TYPE DISK TO '/backup/autobackup/%F';
  CONFIGURE RETENTION POLICY TO RECOVERY WINDOW OF 7 DAYS;
  CONFIGURE BACKUP OPTIMIZATION ON;

  BACKUP AS COMPRESSED BACKUPSET DATABASE PLUS ARCHIVELOG DELETE INPUT;

  CROSSCHECK BACKUP;
  DELETE NOPROMPT OBSOLETE;
}
```
![Alt text](18.png)
![Alt text](19.png)

## 总结与心得
通过设计和实施这个数据库备份方案，我们能够确保数据的安全性和可恢复性。以下是一些总结与心得：

1. 归档日志模式的使用：将数据库切换到归档日志模式是一种重要的安全措施。它确保了所有的日志记录都被保存下来，包括事务的细节和更改的历史。这样，在备份时，我们可以包含所有的数据更改，以便在需要时进行恢复操作。

2. RMAN备份工具的优势：RMAN是Oracle提供的强大备份和恢复工具。通过编写备份脚本，我们能够自动化备份过程，并配置各种选项来满足特定需求。RMAN的灵活性和高效性使得备份过程更加可靠和高效。

3. 备份集管理和优化：我们定义了备份集的保留策略，这有助于管理备份文件的存储空间和保留时间。同时，备份优化设置可以提高备份的效率，减少备份过程的时间和资源消耗。这些设置可以根据具体需求进行调整，以达到最佳的备份性能。

4. 数据恢复能力的保证：备份是为了能够在需要时进行数据恢复。通过定期备份整个数据库，我们可以确保在发生故障或数据丢失时，能够恢复到最近的可靠状态。备份文件的存储和管理要得当，以便在恢复操作时能够快速访问并恢复数据。

综上所述，一个完善的数据库备份方案是确保数据安全性和可恢复性的关键。通过合理的设置和策略，我们能够保护数据库免受数据丢失和故障的影响，并在需要时快速恢复数据，确保业务的连续性和可靠性。

